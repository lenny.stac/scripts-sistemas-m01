#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# for-notes: notes ...
#	
# suspès,aprpvat,excel·lent
#------------------------------------------------------------------
ERR_ARGS=1
ERR_NOTA=2
#1) Validar arguments
if [ $# -eq 0 ]
then 
	echo “Error:num arg incorrecte”
	echo ”Usage: $0 nota ...”
	exit $ERR_ARGS
fi  

#2)validar nota
for nota in  $*
do
	if ! [ $nota -ge 0 -a $nota -le 10 ]
	then
		echo "Error: nota $nota no valida (0-10)" >> /dev/stderr
	else
		if [ $nota -lt 5 ]
		then
			echo "suspes"
		elif [ $nota -lt 7 ]
		then
			echo "aprovat"
		else
		echo "notable"
		fi	
	fi
done
exit $ERR_NOTA
 


