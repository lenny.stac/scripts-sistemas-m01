#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# Validar nota : suspès , aprovat
#	a) rep un argument
#	b) és del 0 al 10
#----------------------------------------------------------------------
ERR_NARGS=1
ERR_NOTA=2
#1) Validar args
if [ $# -ne 1 ]
then 
	echo "ERROR:num Args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi
#2) Validar nota [0,10]
if ! [ $1 -ge 0 -a $1 -le 10 ] #[ $1 -gt 10 ]
then 
	echo "ERROR: nota $1 no valida "
	echo "nota pren valors del 0 al 10"
	echo "Usage: $0 nota"
	exit $ERR_NOTA
fi
# 3) XIXA

nota=$1
if [ $nota -lt 5 ]
then
	echo "nota: $nota Suspés"
elif [ $nota -lt 7 ]
then
	echo "nota: $nota Aprovat"
elif [ $nota -lt 9 ]
then
	echo "nota: $nota Notable"
else
	echo "nota: $nota Exelent"
fi
exit 0


