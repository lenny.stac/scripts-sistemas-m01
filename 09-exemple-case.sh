#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# Sistemes Operatius
#	
# Exemple case
#------------------------------------------------------------------
#2) dilllund,dimarts,dimecres,dijous ---> laborable
# disabte ,diumenge --> festiu
case $1 in 
	"dl"|"dt"|"dj"|"dv")
		echo "$1 es laborable"
		;;
	"ds"|"dm")
		echo "$1 es festiu"
		;;
	*)
		echo "aixó ($1) no és un dia  "
		;;
esac
exit 0
#1)exemple vocals
case $1 in
	[aeiou])
		echo "$1 es una vocal"
		;;
	[bcdfghjklmnpqrstvwyzx])
		echo "$1 és una consonant"
		;;
	*)
		echo "$1 es una altra cosa"
		;;		
esac


