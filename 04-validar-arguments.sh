#! /bin / bash
#
# Lenny Santa Cruz 
#
#	Validar que te 2 args i mostrar-los
#
#
# 04-validar -arguments.sh nom cognom
#----------------------------------------------------------------------
#1)vALIDAR ARGUMENTS
if [ $# -ne 2 ]
then
	echo "ERROR:num args incorrecte"
	echo "Usage: $0 nom cognom"
	exit 1
fi


#2)XIXA
nom=$1 
cognom=$2

echo "nom: $nom "
echo "cognom: $cognom"
