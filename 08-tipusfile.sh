#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# $ prog file
#	
# Indicar si dir és : regular ,dir , link , o altre cosa
#------------------------------------------------------------------
ERR_ARGS=1
ERR_NOEXIST=2
#1) Validar arguments
if [ $# -ne 1 ]
then 
	echo “Error:num arg incorrecte”
	echo ”Usage: $0 /file /directory /link”
	exit ARR_ARGS
fi  

#2) XIXA
file=$1
if [ ! -e $file ]
then
	echo "$file no existeix"
	exit $ERR_NOEXIST
elif [ -f $file ]
then
	echo ”$file es un regular file “
elif [ -h $file ]
then
	echo “$file es un link ”
elif [ -d $fiel ]
then
	echo ”$file es un directori  ”
else
	echo "$file és una altra cosa"
fi
exit 0
