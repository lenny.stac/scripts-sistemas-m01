#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar _Arguments_exercici_1
#
#	Processar els arguments i mostra per stdout nómes els 4 o mes caracters
#
#------------------------------------------------------------------------------------------

while read -r line
do
	nom=$(echo $line | cut -c1)
	cognom=$(echo $line | cut -d' ' -f1)
	echo "$nom $cognom"
done
exit 0


