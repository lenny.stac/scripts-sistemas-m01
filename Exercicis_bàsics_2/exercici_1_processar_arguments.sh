#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar _Arguments_exercici_1
#
#	Processar els arguments i mostra per stdout nómes els 4 o mes caracters
#
#------------------------------------------------------------------------------------------

for arg in $*
do
	echo "$arg"| grep  -E ".{4,}" 
done
exit 0

