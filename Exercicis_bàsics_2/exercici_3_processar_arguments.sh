#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar_Arguments_exercici_3
#
#	Processar els arguments i mostra per stdout nómes els 4 o mes caracters
#
#------------------------------------------------------------------------------------------
cont=0
for matricula in $*
do
	echo "$matricula"| grep -q -E "[0-9]{4}-[A-Z]{3}"
	if ! [ $? -eq 0 ]
	then
		echo "$matricula no valid" >> /dev/stderr
		((cont++))
	fi
done
exit $cont

