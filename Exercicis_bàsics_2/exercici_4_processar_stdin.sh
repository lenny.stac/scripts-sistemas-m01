#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar _Arguments_exercici_1
#
#	Processar els arguments i mostra per stdout nómes els 4 o mes caracters
#
#------------------------------------------------------------------------------------------

ok=0
num=1

while read -r line
do
	echo "$num:$line" | tr '[:lower:]' '[:upper:]'
	((cont++))
done
exit $ok

