#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar _Arguments_exercici_1
#
#	Processar els arguments i mostra per stdout nómes els 4 o mes caracters
#
#------------------------------------------------------------------------------------------

while read -r line
do
	caracter=$(echo $line |wc -c)
	if [ $caracter -lt 50 ]
	then
		echo "$line"
	fi

exit 0

