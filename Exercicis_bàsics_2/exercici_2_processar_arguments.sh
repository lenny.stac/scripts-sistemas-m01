#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	Processar _Arguments_exercici_2
#
#	Processar els arguments i comptar quantes n'hin ha se 3 o més caracters.
#
#------------------------------------------------------------------------------------------
carcater3=0
for arg in $*
do
	echo "$arg"| grep -q -E ".{3,}"
	if [ $? -eq 0 ]
	then
		((caracter3++))
	fi	
done
echo "total d'arguments amb mes de 3 caracters: $caracter3"
exit 0

