#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# 
#	Exemples while	
# 
#------------------------------------------------------------------

#8) processar linia a linia un file
FileIn=$1
num=1
while read -r line
do
	chars=$(echo "$line" | wc -c )
	echo "$num: ($chars) $line"|tr '[:lower:]' '[:upper:]'
	((num++))
done < $FileIn
exit 0

#7)numera i mostrar en majuscules
num=1
while read -r line
do
	echo "$num:$line"| tr '[:lower:]' '[:upper:]'
	((num++))
done
exit 0


#6)iterar linia a linia fins a token (Exemple FI)
num=1
TOKEN="FI"
read -r line
while [ "$line" != $TOKEN ]
do
	echo "$num: $line"
	read -r line
	((num++))
done
exit 0

#5)numerar les linies rebudes 

num=1
while read -r line
do
	echo "$num:$line"
	((num ++))
done
exit 0

#4)procersar l'entrada estandar lina a linia
while read -r line
do
	echo "$line"
done
exit 0


#3) iterar la lista d'arguments amb while ( per poder aprobar fer-ho amb FOR)
while [ -n "$1" ]
do
	echo "$1 $# $*"
	shift
done
exit 0


#2) comptador decrementa valor rebut
min=0
num=$1
while [ $num -ge $min ]
do
	echo "$num"
	((num--))
done
exit 0




#1) Mostrar numeros del 1 al 10
max=10
num=1
while [ $num -le $max ]
do
	echo -n "$num "
	((num++))
done
exit 0 
