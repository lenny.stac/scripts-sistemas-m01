#! /bin/bash
# 
#
# 	M01 SISTEMAS
#
# Lenny Santa Cruz
#
#
# Gener 2024
#
# Exemple ordre if
#
# -------------------------------------------------------------------------
#
#1)vALIDAR ARGUMENTS
if [ $# -ne 1 ]
then
	echo "ERROR:num args incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

#2) XIXA

edat=$1

if [ $edat -lt 18 ]
then
	echo "edat $edat és menor d'edat"
else
	echo "edat $edat és mmajor'edat"
fi
exit 0


