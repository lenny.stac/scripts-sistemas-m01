#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# Descripcio: exemple bucle for
#------------------------------------------------------------------
#8)iterar i numerar per login de /etc/passwd alfabeticament
#cut -d: -f1 /etc/passwd | sort

login=$(cut -d: -f1 /etc/passwd|sort)
num=1
for log in $login
do
	echo "$num:$log"
	((num++))
done 
exit 0

#7) llistar numerant les lines
num=1
llistat=$(ls)
for fit  in $llistat
do
	echo "$num:$fit"
	((num++))
done
exit 0

#6)iterar per cad un dels valor del lordre ls
llistat=$(ls)
for fit  in $llistat
do
	echo "$fit"
done
exit 0


#5) numera args
num=1
for arg in $@
do 
	echo "$num:$arg "
	((num++))
done 
exit 0
#4) $@  permet expansio encara que estigui entre "" $* no permet expancio amb ""
for arg in "$@" 
do
	echo "$arg"
done
exit 0


# 3) iterar per una llista d'arguments
for arg in $*
do 
	echo "$arg"
done 
exit 0

#2)iterar noms
for nom in "pere marta anna pau"
do
	echo "$nom"
done
exit 0
#1) iterar noms
for nom in "pere" "marta" "anna" "pau"
do
	echo "$nom"
done
exit 0
