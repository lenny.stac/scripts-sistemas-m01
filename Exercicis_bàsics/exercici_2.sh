#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercici_2
#
#	Mostra els arguments rebuts linia a linia , tot numerànt-los
#
#
#-----------------------------------------------------------------------


#2.1) Fet amb For
num=1

for arg in $*
do
	echo "$num: $arg"
	((num++))
done
exit 0
