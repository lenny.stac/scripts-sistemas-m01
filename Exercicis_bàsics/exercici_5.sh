#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercici_5
#
#	Mostra linia a linia l'entrada estàndar, retallant els primer 
#	50 caracter	
#
#-----------------------------------------------------------------------
while read -r line
do
	echo "$line" | cut -c-50
done 
exit 0
