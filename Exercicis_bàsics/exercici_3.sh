#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercici_3
#
#	Fer un comptardor des de zero fins el valor inicat per
#	l'argument rebut
#
#-----------------------------------------------------------------------

num=0
MAX=$1
while [ $num -le $MAX  ]
do
	echo "$num"
	((num++))
done 
exit 0
