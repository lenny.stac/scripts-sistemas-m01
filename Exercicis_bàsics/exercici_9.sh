#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	Exercici_9
#
#	Fer un programa que rep per stdin noms d'usuari (un per linia), 
#	si existeixen en el sistema (en el fitxer /etc/passwd) mostra 
#	el nom per stdout .Si no existeix el mostra per stderr
#----------------------------------------------------------------------

while read -r line 
do
	grep -q "$line" /etc/passwd
	if [ $? -eq 0 ]
	then
		echo "$line"
	else
		echo "$line no existeix" >> /dev/stderr
	fi

done 
exit 0

