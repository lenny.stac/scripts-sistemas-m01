#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercicic_8
#
#	Fer un programa que rep com a argument noms d'usuari, si existeixen
#	en el sistema (en fitxer /etc/passwd) mostra el nom per stdout, si
#	no existeix el mostra per stderr
#----------------------------------------------------------------------------
ERR_ARGS=1

if [ $# -lt 1 ]
then
	echo "Error: num Args incorrecte"
	echo "Usage: $0 user"
	exit $ERR_ARGS
fi



for user in $*
do
	grep -q "^$user:" /etc/passwd
	if [ $? -eq 0 ]
	then
		echo "$user" 
	else
		echo "$user no existeix" >> /dev/stderr
	fi
done
exit 0
