#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	execici_1
#
#	Mostra l'entrada estandar linia a linia
#
#-----------------------------------------------------------------------

num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0
