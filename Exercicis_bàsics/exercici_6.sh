#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#	
#	exercici_6
#
#	Fer un programa que rep com a argument noms de dies de la setmana
#	i mostra quants dies eren laborables i quants festius. Si l'argument
#	no és un dia de la semana genera yn error per stderr
#
#------------------------------------------------------------------------------

laborable=0
no_laborable=0
for dies in $*
do
	case $dies in
		"dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
			((laborable++))
			;;
		"disabte"|"diumenge")
			((no_laborable++))
			;;
		*)
			echo "aixó $dies no és un dia" >> /dev/stderr
			;;
	esac
done
echo "total de dies que son laborables: $laborable"
echo "total de dies festius: $no_laborable"
exit 0
