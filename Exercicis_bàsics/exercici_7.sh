#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercici_7
#
#	Processar linia a linia l'entrada estàndar , si te mès de 60
#	caracters,si no no
#
#---------------------------------------------------------------------- 
while read -r line 
do
	num=$(echo "$line" | wc -c )
	if [ "$num" -gt 60 ]
	then 
		echo "$line"
	fi

done 
exit 0
