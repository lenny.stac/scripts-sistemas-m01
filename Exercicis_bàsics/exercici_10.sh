#! /bin/bash
# 
# 	Lenny Santa Cruz
#
#	Febrero 2024
# 
#	exercici_10
#
#	Fer un programa que rep com a argument un número indicatiu del número màxim de
#  	linies a mostrar. El programa processa stdin línia a linia i mostra numrades
#  	un màxim de num linies.
#------------------------------------------------------------------------------------------
ERR_NARGS=1

if [ $# -ne 1 ]
then
echo "Error: num arg incorrecte"
echo "Usage: $0 num"
exit $ERR_NARGS
fi

MAX=$1
compt=1

read -r line
while [ $MAX -gt 0 ]
do
	echo "$compt: $line"
	read -r line
	((compt++))
	((max--))
done

exit 0
