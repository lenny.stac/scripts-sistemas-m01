#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	exercici_4
#
#	Fer un programa que rep com a arguments números de més (un o més)
#	i indica per cada rebut quants dies té el mes
#
#-----------------------------------------------------------------------
ERR_ARG=1

# validar argument
if [ $# -lt 1 ]
then 
	echo "Error : num Arg incorrecte"
	echo "Usage: $0 mes (1-12)"
	exit $ERR_ARG
fi
#xixa
for mes in $*
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]
	then
		echo "Error: invalid $mes" >> /dev/stderr
	else
		case $mes in 
			"4"|"6"|"9"|"11")
				echo "$mes té 30 dies"
				;;
			"2")
				echo "$mes té 28 o 29 dies"
				;;
			*)
				echo "$mes té 31 dies"
		esac
	fi
done
exit 0
