#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#
#	a)rep un arg i és un directori i es llista
#	b)llista numerant els elements del dir
#---------------------------------------------------------------------

ERR_NARG=1
ERR_NODIR=2
#1) validar que te un argument
if [ $# -ne 1 ]
then 
	echo "Error:num arg incorrecte"
	echo "Usage:$0 dir"
	exit $ERR_NARG
fi

dir=$1
#2)validar que és un dir
if ! [ -d $dir ]
then
	echo "Error: $dir no és un directori"
       	echo "Usage: $0 dir"
	exit $ERR_NODIR
fi
#3)xixa:llista
num=1
llista=$(ls $dir)
for linia in $llista
do
	echo "$num:$linia"
	((num++))
done
exit 0



