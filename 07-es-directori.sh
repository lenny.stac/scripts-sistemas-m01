#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# Llistar el directori rebut
#	a) Verificar rep un arg 
#	b) veroficacio que és un directori
#------------------------------------------------------------------
ERR_NARGS=1
ERR_NOTA=2
#1) Validar args
if [ $# -ne 1 ]
then 
	echo "ERROR:num Args incorrecte"
	echo "Usage: $0 dir"
	exit $ERR_NARGS
fi
#2) Validar si es un directori
if [ ! -d $1 ]
then 
	echo "ERROR: $1 no es directori"
	echo "Usage: $0 dir"
	exit $ERR_NOTA
fi

#3) xixa

dir=$1

if [ -d $dir ]
then 
	ls -l $dir
fi
exit 0

