#! /bin/bash
#
# 	Lenny Santa Cruz
#
#	Març 2024
#
#	Funcions
#----------------------------------

function suma(){
	echo "$(($1+$2))"
	return 0
}

function showUserByLogin(){
	login=$1
	line=$(grep "^$login:" /etc/passwd)

	if [ -z "$line" ]
	then
		echo "Error..."
		return 1
	fi
	login=$(echo $line|cut -d: -f1)
	uid=$(echo $line |cut -d: -f3)
	gid=$(echo $line |cut -d: -f4)
	shell=$(echo $line|cut -d: -f7)
	echo "login: $login"
	echo "uid: $uid"
	echo "gid: $gid"
	echo "shell: $shell"
	return 0
}

function showUsersInGroupByGid(){
	gid=$1
	gname=$(grep "^[^.]*:[^.]*:$gid:" /etc/group|cut -d: -f1 )

	if [ -z "$line" ]
	then
		echo "Error..."
	else
		echo "$gname($gid)"
	fi



	user=$(grep "^[^.]*:[^.]*:[^.]*:$gid:" /etc/passwd)
	login=$(echo $user | cut -d: -f1)
	uid=$(echo $user |cut -d: -f3)
	shell=$(echo $user|cut -d: -f7)

	echo "login: $login"
	echo "uid: $uid"
	echo "shell: $shell"
 
return 0
}

function showAllShells(){
	MIN=$1
	llista=$(cut -d: -f7 /etc/passwd |sort -u)
	for shell in $llista
	do
		num=$(grep ":$shell$" /etc/passwd|wc -l)
		if [ $num -gt $MIN ]
		then
			echo "shell :($num) $shell "
			grep -E ":$shell$" /etc/passwd | cut -d: -f1 | sed -r 's/^(.*)$/\t\1/'
		fi
	done
	return 0
}
