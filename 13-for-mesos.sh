#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# for-notes: notes ...
#	
# suspès,aprpvat,excel·lent
#------------------------------------------------------------------
ERR_ARGS=1
ERR_NOTA=2
#1) Validar arguments
if [ $# -eq 0 ]
then 
	echo “Error:num arg incorrecte”
	echo ”Usage: $0 mes”
	exit $ERR_ARGS
fi  

#2)validar nota
for mes in  $*
do
	if ! [ $mes -ge 1 -a $mes -le 12 ]
	then
		echo "Error: mes $mes no valid (1-12)" >> /dev/stderr
	else
		if [ $nota -lt 5 ]
		then
			echo "suspes"
		elif [ $nota -lt 7 ]
		then
			echo "aprovat"
		else
		echo "notable"
		fi	
	fi
done
exit $ERR_NOTA
 


