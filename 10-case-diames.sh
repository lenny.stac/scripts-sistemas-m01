#! /bin / bash
#
# Lenny Santa Cruz 
#
# Febrer 2024
#
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
#	1)validar rep un rang
#	2)validar mes [1-12]
#	3)xixa
#------------------------------------------------------------------
ERR_ARGS=1
ERR_MES=2
#validar argumnet
if [ $# -ne 1 ]
then 
	echo "ERROR: num args incorrecte"
	echo "Usage: $0 mes"
	exit $ERR_ARGS
fi
#validar mes

if ! [ $1 -ge 1 -a $1 -le 12 ]
then
	echo "ERROR: arg incorrecte"
    	echo "Usage : $0 [1-12]"
	exit $ERR_MES
fi

#xixa
mes=$1

case $mes in 
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo "$mes té 31 dies"
		;;
	"4"|"6"|"9"|"11")
		echo "$mes té 30 dies"
		;;
	"2")
		echo "$mes té 28 0 29 dies"
		;;
esac
exit 0
