#! /bin/bash
#
#	Lenny Santa Cruz
#
#	Febrer 2024
#
#	prog dir ......
#
#	a)rep un arg i és un directori i es llista.
#	b)llista numerant els elements del dir.
#	c)per cada element dir si es dir,regular, o altra cosa.
#	d)es reben n directoris
#---------------------------------------------------------------------
ERR_NARG=1
ERR_NODIR=2
#1) validar que te un argument

if [ $# -eq 0 ]
then 
	echo "Error:num arg incorrecte"
	echo "Usage:$0 dir"
	exit $ERR_NARG
fi

#2) iterar per cada argument
for dir in $*
do
	if ! [ -d $dir ]
	then
	echo "Error: $dir no és un directori" >> /dev/stderr
else
	llista=$(ls $dir)
	echo "llistat: $dir -------------------"
	for linia in $llista
	do
		if [ -d $dir/$linia ]
		then
			echo -e  "\t$linia is a directory"
		elif [ -f $dir/$linia ]
		then
			echo -e "\t$linia is a regular file"
		else
			echo -e "\t$linia is another thing"
		fi
	done
	fi
done
exit 0



