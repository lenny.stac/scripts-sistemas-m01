#! /bin/bash
#
# Lenny Santa Cruz
#
#
# processa matricules
#------------------------------------------------------
fileIn=$1
status=0

while read -r line
do
	echo $line | grep -E "^[A-Z,a-z]{4}[0-9]{3}$"
	if [ $? -ne 0 ]  
	then
		echo "$line no valid" >> /dev/stderr
		status=3
	fi

done < $fileIn
exit $status
